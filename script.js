"use strict";

function calculator(first, second, action) {
  let total = 0;

  while (isNaN(first) || isNaN(second) || first === "" || second === "") {
    first = prompt("Enter the first number again", first);
    second = prompt("Enter the second number again ", second);
  }

  while (action !== "+" && action !== "-" && action !== "/" && action !== "*") {
    action = prompt("Enter the action again(+, -, *, /)");
  }

  if (action === "+") {
    total = Number(first) + Number(second);
  } else if (action === "-") {
    total = Number(first) - Number(second);
  } else if (action === "*") {
    total = Number(first) * Number(second);
  } else if (action === "/") {
    total = Number(first) / Number(second);
  }

  return console.log(`The result of ${first} ${action} ${second} = ${total}`);
}

calculator(prompt("Enter the first number"), prompt("Enter the second number"), prompt("Enter the action"));  
